# Load balance with Docker
### Stop and destroy all containers

> `docker stop $(docker ps -aq) && docker rm $(docker ps -aq)`

### Build images and start the containers

> `docker-compose up -d --build`