DROP TABLE IF EXISTS conta;
 
CREATE TABLE conta (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  descricao VARCHAR(250) NOT NULL,
  valor LONG NOT NULL,
);

INSERT INTO conta (descricao, valor) VALUES
  ('Conta de luz', 15.00),
  ('Conta de agua', 20.00),
  ('Conta noraml', 200.00);
  
 DROP TABLE IF EXISTS veiculo;
 
CREATE TABLE veiculo (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  codigo VARCHAR(250) NOT NULL,
  placa VARCHAR(250) NOT NULL,
);

INSERT INTO veiculo (codigo, placa) VALUES
  ('0001','NUX5555'),
  ('0002', 'HVZ3983'),
  ('0003', 'OCE4567'),
  ('0004', 'PMS3333');
  
  