package br.com.compare.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.compare.model.Pendencia;
import br.com.compare.model.Veiculo;


@RestController
@RequestMapping("/api/rest")
public class PendenciaController {

	private Map<Long, Pendencia> listaPendencias;

	public PendenciaController() {
		listaPendencias = new HashMap<Long, Pendencia>();

		Veiculo v1 = new Veiculo(1L, "NUX5555", "Mitsubishi ASX", Veiculo.CARRO);
		Veiculo v2 = new Veiculo(2L, "PMS3333", "Honda Fit", Veiculo.CARRO);
		Veiculo v3 = new Veiculo(3L, "OCE4567", "Volkswagen Fox", Veiculo.CARRO);
		Veiculo v4 = new Veiculo(4L, "HVZ3983", "Honda Titan", Veiculo.MOTO);

		Pendencia p1 = new Pendencia(1L, "Multa por excesso de Velocidade", 150.00, v1, Pendencia.PENDENTE);
		Pendencia p2 = new Pendencia(2L, "Multa por estacionamento proibido", 90.00, v2, Pendencia.PENDENTE);
		Pendencia p3 = new Pendencia(3L, "IPVA 2018", 1500.00, v3, Pendencia.RESOLVIDA);
		Pendencia p4 = new Pendencia(4L, "Licenciamento 2019", 300.00, v4, Pendencia.PENDENTE);

		listaPendencias.put(p1.getId(), p1);
		listaPendencias.put(p2.getId(), p2);
		listaPendencias.put(p3.getId(), p3);
		listaPendencias.put(p4.getId(), p4);
	}

	@RequestMapping(value = "/pendencias", method = RequestMethod.GET)
	public ResponseEntity<List<Pendencia>> listarPendencias() {
		return new ResponseEntity<List<Pendencia>>(new ArrayList<Pendencia>(listaPendencias.values()), HttpStatus.OK);
	}

	@RequestMapping(value = "/pendencia/{codigo}", method = RequestMethod.GET)
	public ResponseEntity<Pendencia> buscar(@PathVariable("codigo") Long codigo) {

		Pendencia pendencia = listaPendencias.get(codigo);
		if (pendencia == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		return new ResponseEntity<Pendencia>(pendencia, HttpStatus.OK);
	}

	@RequestMapping(value = "/pendencia/{codigo}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deletar(@PathVariable("codigo") Long codigo) {

		Pendencia pendencia = listaPendencias.remove(codigo);

		if (pendencia == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/pendencia", method = RequestMethod.POST)
	public ResponseEntity<?> criar(@RequestBody Pendencia pendencia) {

		if (pendencia != null) {
			listaPendencias.put(pendencia.getId(), pendencia);
			return new ResponseEntity<String>(HttpStatus.CREATED);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/regularizarPendencia/{codigo}", method = RequestMethod.POST)
	public ResponseEntity<?> regularizar(@PathVariable("codigo") Long codigo) {

		Pendencia pendencia = listaPendencias.get(codigo);
		if (pendencia == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		else {
			pendencia.setValor(0.0);
			pendencia.setStatus(Pendencia.RESOLVIDA);
		}

		return new ResponseEntity<Pendencia>(pendencia, HttpStatus.OK);
	}


}
