package br.com.compare.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.compare.model.Pendencia;
import br.com.compare.model.Veiculo;
import restql.core.RestQL;
import restql.core.response.QueryResponse;

@RestController
@RequestMapping("/api/restql")
public class RestQLController {
	
	@Autowired
	private RestQL restQL;


	/*
		GET /api/rest/pendencias -> Lista pendencias
		GET /api/rest/pendencia/{codigo} -> buscar pendencia por codigo
		GET /api/rest/veiculo -> Lista veiculos
		GET /api/rest/veiculo/{placa} -> buscar veiculo por placa
	 */


	@GetMapping(value = "/pendencias", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Pendencia>> index() {

		QueryResponse response = this.restQL.executeQuery("from pendencias");

		List<Pendencia> pendencias = response.getList("pendencias", Pendencia.class);

		return new ResponseEntity<List<Pendencia>>(pendencias, HttpStatus.OK);
	}


	@RequestMapping(value = "/pendencia/{codigo}", method = RequestMethod.GET)
	public ResponseEntity<Pendencia> buscar(@PathVariable("codigo") Long codigo) {

		QueryResponse response = this.restQL.executeQuery("from pendencia with codigo = ?", codigo);

		Pendencia pendencia = response.get("pendencia", Pendencia.class);

		return new ResponseEntity<Pendencia>(pendencia, HttpStatus.OK);
	}


	@RequestMapping(value = "/veiculos", method = RequestMethod.GET)
	public ResponseEntity<List<Veiculo>> listarVeiculos() {

		QueryResponse response = this.restQL.executeQuery("from veiculos");

		List<Veiculo> veiculos = response.getList("veiculos", Veiculo.class);

		return new ResponseEntity<List<Veiculo>>(veiculos, HttpStatus.OK);
	}


	@RequestMapping(value = "/veiculo/{placa}", method = RequestMethod.GET)
	public ResponseEntity<Veiculo> buscar(@PathVariable("placa") String placa) {

		QueryResponse response = this.restQL.executeQuery("from veiculo with placa = ?", placa);

		Veiculo veiculo = response.get("veiculo", Veiculo.class);

		return new ResponseEntity<Veiculo>(veiculo, HttpStatus.OK);
	}
}