package br.com.compare.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.compare.model.Veiculo;

@RestController
@RequestMapping("/api/rest")
public class VeiculoController {

	private Map<String, Veiculo> listaVeiculos;

	public VeiculoController() {
		listaVeiculos = new HashMap<String, Veiculo>();

		Veiculo v1 = new Veiculo(1L,"Mitsubishi ASX", "NUX5555", Veiculo.CARRO);
		Veiculo v2 = new Veiculo(2L, "Honda Fit", "PMS3333", Veiculo.CARRO);
		Veiculo v3 = new Veiculo(3L, "Volkswagen Fox", "OCE4567", Veiculo.CARRO);
		Veiculo v4 = new Veiculo(4L, "Honda Titan", "HVZ3983", Veiculo.MOTO);

		listaVeiculos.put(v1.getPlaca(), v1);
		listaVeiculos.put(v2.getPlaca(), v2);
		listaVeiculos.put(v3.getPlaca(), v3);
		listaVeiculos.put(v4.getPlaca(), v4);
	}

	@RequestMapping(value = "/veiculos", method = RequestMethod.GET)
	public ResponseEntity<List<Veiculo>> listarVeiculos() {
		return new ResponseEntity<List<Veiculo>>(new ArrayList<Veiculo>(listaVeiculos.values()), HttpStatus.OK);
	}

	@RequestMapping(value = "/veiculo/{placa}", method = RequestMethod.GET)
	public ResponseEntity<Veiculo> buscar(@PathVariable("placa") String placa) {

		Veiculo veiculo = listaVeiculos.get(placa);
		if (veiculo == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		return new ResponseEntity<Veiculo>(veiculo, HttpStatus.OK);
	}

	@RequestMapping(value = "/veiculo/{placa}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deletar(@PathVariable("placa") String placa) {

		Veiculo veiculo = listaVeiculos.remove(placa);

		if (veiculo == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/veiculo", method = RequestMethod.POST)
	public ResponseEntity<?> criar(@RequestBody Veiculo veiculo, UriComponentsBuilder ucBuilder) {

		if (veiculo != null) {
			listaVeiculos.put(veiculo.getPlaca(), veiculo);
			return new ResponseEntity<String>(HttpStatus.CREATED);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

}
