package br.com.compare.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.compare.graphql.schema.MutationResolver;
import br.com.compare.graphql.schema.QueryResolver;
import br.com.compare.model.Veiculo;

@RestController
@RequestMapping("/api/graphql/veiculo")
public class VeiculoResource {
    
	@Autowired
    private QueryResolver query;
	
    @Autowired
    private MutationResolver mutation;


    @GetMapping
    @ResponseBody
    public List<Veiculo> get() {
        return query.getAllVeiculos();
    }

    @GetMapping(value = "{id}")
    @ResponseBody
    public Veiculo get(@PathVariable("id") Long id) {
        return query.getVeiculo(id);
    }

    @PostMapping
    @ResponseBody
    public void createVeiculo(String codigo, String placa) {
        mutation.addVeiculo(codigo, placa);
    }
}