package br.com.compare.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.compare.model.Conta;

@Service
public class FinanceiroService {
	
	public List<Conta> recuperarContasNegativadas() {
		return new ArrayList<Conta>();
	}
	
	public List<Conta> recuperarContasPositivas() {
		return new ArrayList<Conta>();
	}
}