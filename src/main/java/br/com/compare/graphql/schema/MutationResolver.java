package br.com.compare.graphql.schema;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;

import br.com.compare.exception.VeiculoNotFoundException;
import br.com.compare.model.Veiculo;

@Component
public class MutationResolver implements GraphQLMutationResolver {

    @Autowired
    private VeiculoRepository veiculoRepository;

    // addVeiculo(codigo: String!, placa: String!) : Veiculo!
    @Transactional
    public Veiculo addVeiculo(String modelo, String placa) {
    	Veiculo v = new Veiculo();
    	v.setModelo(modelo);
    	v.setPlaca(placa);
        return veiculoRepository.saveAndFlush(v);
    }

    @NotNull
    private Veiculo getVeiculoById(Long veiculoId) {
        return veiculoRepository.findById(veiculoId)
                .orElseThrow(() -> new VeiculoNotFoundException(veiculoId));
    }
}
