package br.com.compare.graphql.schema;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.compare.model.Veiculo;

public interface VeiculoRepository extends JpaRepository<Veiculo, Long> {
    Optional<Veiculo> findById(Long id);
}