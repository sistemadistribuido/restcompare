package br.com.compare.graphql.schema;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;

import br.com.compare.model.Veiculo;

@Component
public class QueryResolver implements GraphQLQueryResolver {

    @Autowired
    private VeiculoRepository veiculoRepository;

    //allVeiculos: [Veiculo]
    public List<Veiculo> getAllVeiculos() {
        return veiculoRepository.findAll();
    }

    //veiculo(id: Long!): Veiculo
    public Veiculo getVeiculo(Long id) {
        return veiculoRepository.findById(id).get();
    }
}
