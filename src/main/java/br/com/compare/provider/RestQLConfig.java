package br.com.compare.provider;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import restql.core.RestQL;
import restql.core.config.ClassConfigRepository;
import restql.core.query.QueryOptions;

@Configuration
public class RestQLConfig {

    @Bean
    public RestQL getRestQL() {

    	Map<String, String> map = new HashMap<>();

    	ClassConfigRepository config = new ClassConfigRepository(map);
    	config.put("pendencias", "http://localhost:8080/restcompare/api/rest/pendencias");
    	config.put("pendencia", "http://localhost:8080/restcompare/api/rest/pendencia/:codigo");
    	config.put("veiculos", "http://localhost:8080/restcompare/api/rest/veiculos");
    	config.put("veiculo", "http://localhost:8080/restcompare/api/rest/veiculo/:placa");

		QueryOptions queryOptions = new QueryOptions();
		queryOptions.setTimeout(100000);
		queryOptions.setGlobalTimeout(100000);
		queryOptions.setDebugging(true);
		return new RestQL(config, queryOptions);
    }
}