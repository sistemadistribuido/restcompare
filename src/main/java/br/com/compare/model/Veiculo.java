package br.com.compare.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@EqualsAndHashCode
@Table(name = "veiculo")
@Entity
public class Veiculo {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "codigo", nullable = false)
    private String codigo;
    
    @Transient
    @Column(name = "modelo", nullable = false)
    private String modelo;

    @Column(name = "placa", nullable = false)
    private String placa;

    @Transient
    @Column(name = "tipo", nullable = false)
    private String tipo;
    
    //@OneToMany
    //@JoinColumn(name="veiculo_id")
    //private List<Pendencia> pendencias;
    
    public final static String CARRO = "1";
	public final static String MOTO = "2";
	
	public Veiculo(Long id, String modelo, String placa, String tipo) {
		super();
		this.id = id;
		this.modelo = modelo;
		this.placa = placa;
		//this.pendencias = pendencias;
		this.tipo = tipo;
	}
	
	public Veiculo() {
		super();
	}


}
