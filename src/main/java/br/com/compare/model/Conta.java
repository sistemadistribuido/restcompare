package br.com.compare.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Conta {
	@Id
	private Integer id;
	private String descricao;
	private Integer valor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	

}
