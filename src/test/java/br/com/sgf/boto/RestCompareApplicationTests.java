package br.com.sgf.boto;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RestCompareApplicationTests.class)
public class RestCompareApplicationTests {

	@Test
	public void contextLoads() {
	}

}
